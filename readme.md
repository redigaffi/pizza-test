# Pizza Test

## Installing the container

This is the quickest way to get started with the Pizza Test from Vendo. 
If you already have Docker installed you can skip the next steps.
You can also work with the Pizza Test without the included virtual development machine, but you have to configure everything yourself.
NB: Only Mac and Linux host machines 100% supported. If you're on Windows you're on your own..._

### Install Docker
Docker is a software that performs operating-system-level virtualization, also known as "containerization".

Download installation file here (pick the one that matches your host machine environment):

    https://docs.docker.com/install/
    
Before continuing, make sure that the binaries installed are correct (docker and docker-compose).


### Edit hosts file on you host machine
The hosts file enables your computer to find websites that do not have a public DNS entry_

Add the following to /etc/hosts:

    127.0.0.1	pizzatest.vendo.local

### Install pizza test dependencies with composer
To install all the dependencies required by the pizza test, execute the following command in the Pizza Test Directory:

    docker-compose run composer install
        

### Run the stack (using docker-compose)

Execute the following command from the Pizza Test directory:

    docker-compose up


### Visit the Pizza Test website

Type http://pizzatest.vendo.local:8000 in your browser.

You should see a skeleton website with the message:

    Nice! You're set, good luck with the test.

### Last Steps
After finishing with the test, you have to dump your database and copy it into the data/ folder